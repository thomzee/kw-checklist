<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
});

$router->group(['middleware' => 'auth', 'prefix' => 'api/v1'], function () use ($router) {
    # TemplateController
    $router->get('checklists/templates', 'TemplateController@index');
    $router->get('checklists/templates/{id}', 'TemplateController@detail');
    $router->post('checklists/templates', 'TemplateController@store');
    $router->patch('checklists/templates/{id}', 'TemplateController@update');
    $router->delete('checklists/templates/{id}', 'TemplateController@destroy');

    # ChecklistController
    $router->get('checklists', 'ChecklistController@index');
    $router->get('checklists/{id}', 'ChecklistController@detail');
    $router->post('checklists', 'ChecklistController@store');
    $router->patch('checklists/{id}', 'ChecklistController@update');
    $router->delete('checklists/{id}', 'ChecklistController@destroy');

    # ItemController
    $router->post('checklists/{id}/items', 'ItemController@store');
    $router->patch('checklists/{checklistId}/items/{itemId}', 'ItemController@update');
    $router->delete('checklists/{checklistId}/items/{itemId}', 'ItemController@destroy');
    $router->get('checklists/{checklistId}/items/{itemId}', 'ItemController@detail');
    $router->get('checklists/{checklistId}/items', 'ItemController@index');
    $router->post('checklists/complete', 'ItemController@complete');
    $router->post('checklists/incomplete', 'ItemController@incomplete');
});
