<?php


namespace App\Service;


use App\Template;
use Illuminate\Http\Request;

class TemplateService
{
    private $template;

    public function __construct()
    {
        $this->template = new Template();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function paging($limit = 10)
    {
        return $this->template->paginate($limit);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createTemplate(Request $request)
    {
        $template = $this->template->create([
            'name' => $request->input('data.attributes.name'),
            'description' => $request->input('data.attributes.checklist.description'),
            'due_interval' => $request->input('data.attributes.checklist.due_interval'),
            'due_unit' => $request->input('data.attributes.checklist.due_unit')
        ]);

        return $template;
    }

    /**
     * @param Template $template
     * @param Request $request
     */
    public function createTemplateItem(Template $template, Request $request)
    {
        if ($request->input('data.attributes.items.*')) {
            $items = [];
            foreach ($request->input('data.attributes.items.*') as $item) {
                $item['template_id'] = $template->id;
                $items[] = $item;
            }

            $template->templateItems()->insert($items);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->template->findOrFail($id);
    }

    /**
     * @param Template $template
     * @param Request $request
     * @return Template
     */
    public function updateTemplate(Template $template, Request $request)
    {
        $template->update([
            'name' => $request->input('data.attributes.name'),
            'description' => $request->input('data.attributes.checklist.description'),
            'due_interval' => $request->input('data.attributes.checklist.due_interval'),
            'due_unit' => $request->input('data.attributes.checklist.due_unit')
        ]);

        return $template;
    }

    /**
     * @param Template $template
     * @param Request $request
     */
    public function updateTemplateItem(Template $template, Request $request)
    {
        if ($request->input('data.attributes.items.*')) {
            $template->templateItems()->delete();
            $items = [];
            foreach ($request->input('data.attributes.items.*') as $item) {
                $item['template_id'] = $template->id;
                $items[] = $item;
            }

            $template->templateItems()->insert($items);
        }
    }

    /**
     * @param Template $template
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Template $template)
    {
        return $template->delete();
    }
}
