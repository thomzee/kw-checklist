<?php


namespace App\Service;


use App\Checklist;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemService
{
    private $item;

    public function __construct()
    {
        $this->item = new Item();
    }

    public function findByItemId($itemId)
    {
        return $this->item->where('id', $itemId)->firstOrFail();
    }

    public function findByChecklistIdAndItemId($checklistId, $itemId)
    {
        return $this->item->where('checklist_id', $checklistId)->where('id', $itemId)->firstOrFail();
    }

    public function createChecklistItem(Checklist $checklist, Request $request)
    {
        $item = $this->item;
        $item->description = $request->input('data.attribute.description');
        $item->due = $request->input('data.attribute.due');
        $item->urgency = $request->input('data.attribute.urgency');
        $item->assignee_id = $request->input('data.attribute.assignee_id');
        $item->task_id = $request->input('data.attribute.task_id');
        $item->is_completed = $request->input('data.attribute.is_completed');
        $item->completed_at = $item->is_completed ? date('Y-m-d H:i:s') : null;
        $item->created_by = Auth::user()->id;
        $item->checklist_id = $checklist->id;
        $item->save();
    }

    public function updateChecklistItem(Item $item, Request $request)
    {
        $item->description = $request->input('data.attribute.description');
        $item->due = $request->input('data.attribute.due');
        $item->urgency = $request->input('data.attribute.urgency');
        $item->assignee_id = $request->input('data.attribute.assignee_id');
        $item->task_id = $request->input('data.attribute.task_id');
        $item->is_completed = $request->input('data.attribute.is_completed');
        $item->completed_at = $item->is_completed ? date('Y-m-d H:i:s') : null;
        $item->created_by = Auth::user()->id;
        $item->save();
    }

    public function delete(Item $item)
    {
        $item->delete();
    }

    public function changeAllStatus(Request $request, bool $status)
    {
        $updatedData = [];
        foreach ($request->input('data') as $value) {
            $item = $this->findByItemId($value['item_id']);
            $item->is_completed = $status;
            $item->completed_at = $status ? date('Y-m-d H:i:d') : null;
            $item->updated_by = Auth::user()->id;
            $item->save();

            $updatedData[] = $item;
        }
        return $updatedData;
    }
}
