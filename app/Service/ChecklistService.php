<?php


namespace App\Service;


use App\Checklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChecklistService
{
    private $checklist;

    public function __construct()
    {
        $this->checklist = new Checklist();
    }

    public function paging($limit = 10)
    {
        return $this->checklist->paginate($limit);
    }

    public function createChecklist(Request $request)
    {
        $checklist = $this->checklist->create([
            'object_domain' => $request->input('data.attributes.object_domain'),
            'object_id' => $request->input('data.attributes.object_id'),
            'due' => $request->input('data.attributes.due'),
            'urgency' => $request->input('data.attributes.urgency'),
            'description' => $request->input('data.attributes.description'),
            'task_id' => $request->input('data.attributes.task_id')
        ]);

        return $checklist;
    }

    public function createChecklistItem(Checklist $checklist, Request $request)
    {
        if ($request->input('data.attributes.items.*')) {
            $items = [];
            foreach ($request->input('data.attributes.items.*') as $item) {
                $temp = [];
                $temp['checklist_id'] = $checklist->id;
                $temp['description'] = $item;
                $temp['created_by'] = Auth::user()->id;
                $items[] = $temp;
            }

            $checklist->items()->insert($items);
        }
    }

    public function findOrFail($id)
    {
        return $this->checklist->findOrFail($id);
    }

    public function updateChecklist(Checklist $checklist, Request $request)
    {
        $checklist->update([
            'object_domain' => $request->input('data.attributes.object_domain'),
            'object_id' => $request->input('data.attributes.object_id'),
            'due' => $request->input('data.attributes.due'),
            'urgency' => $request->input('data.attributes.urgency'),
            'description' => $request->input('data.attributes.description'),
            'task_id' => $request->input('data.attributes.task_id'),
            'updated_by' => Auth::user()->id
        ]);

        return $checklist;
    }

    public function delete(Checklist $checklist)
    {
        return $checklist->delete();
    }
}
