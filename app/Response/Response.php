<?php


namespace App\Response;


use Symfony\Component\HttpFoundation\JsonResponse;

class Response
{
    public function list($paged, BaseMapper $mapper, array $options = [])
    {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = 'Operation successfully executed.';
        $return['meta']['total'] = $paged->perPage();
        $return['meta']['count'] = $paged->perPage();
        $return['meta']['per_page'] = $paged->perPage();
        $return['meta']['current_page'] = $paged->currentPage();
        $return['meta']['last_page'] = $paged->lastPage();
        $return['meta']['has_more_pages'] = $paged->hasMorePages();
        $return['meta']['self'] = app('request')->fullUrl();
        $return['links']['first'] = $paged->firstItem();
        $return['links']['last'] = $paged->lastItem();
        $return['links']['next'] = $paged->nextPageUrl();
        $return['links']['prev'] = $paged->previousPageUrl();
        $return['data'] = $mapper->list($paged->items());

        foreach($options as $key => $option) {
            $return[$key] = $option;
        }

        return $return;
    }

    public function single($data, BaseMapper $mapper, array $options = [])
    {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = 'Operation successfully executed.';
        $return['meta']['self'] = app('request')->fullUrl();
        $return['data'] = $mapper->single($data);

        foreach($options as $key => $option) {
            $return[$key] = $option;
        }

        return $return;
    }

    public function success()
    {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_OK;
        $return['meta']['message'] = 'Operation successfully executed.';
        $return['data'] = (object) [];
        return $return;
    }

    public function error(\Exception $e)
    {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        $return['meta']['message'] = 'Error executing request.';
        $return['meta']['error'] = $e->getMessage();
        $return['data'] = (object) [];
        return $return;
    }

    public function customError($code, $errors)
    {
        $return = [];
        $return['meta']['code'] = $code;
        $return['meta']['message'] ='Error executing request.';
        $return['meta']['errors'] = $errors;
        $return['data'] = (object) [];
        return $return;
    }

    public function validationFail($errors)
    {
        $return = [];
        $return['meta']['code'] = JsonResponse::HTTP_UNPROCESSABLE_ENTITY;
        $return['meta']['message'] = 'Error executing request.';
        $return['meta']['errors'] = $errors;
        $return['data'] = (object) [];
        return $return;
    }
}
