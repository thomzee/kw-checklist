<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;

class ItemIndexMapper extends BaseMapper implements MapperContract
{
    private $itemMapper;

    public function __construct()
    {
        $this->itemMapper = new ItemMapper();
    }

    public function single($data) {
        $result = [];
        $result['type'] = 'checklists';
        $result['id'] = $data ? $data->id : "";
        $result['attributes']['object_domain'] = $data->object_domain ? $data->object_domain : "";
        $result['attributes']['object_id'] = $data->object_id ? $data->object_id : "";
        $result['attributes']['task_id'] = $data->task_id ? $data->task_id : "";
        $result['attributes']['description'] = $data->description ? $data->description : "";
        $result['attributes']['is_completed'] = $data->is_completed ? $data->is_completed : "";
        $result['attributes']['due'] = $data->due ? $data->due : "";
        $result['attributes']['urgency'] = $data->urgency ? $data->urgency : "";
        $result['attributes']['completed_at'] = $data->completed_at ? $data->completed_at : "";
        $result['attributes']['last_update_by'] = $data->updated_by ? $data->updated_by : "";
        $result['attributes']['updated_at'] = $data->updated_at ? $data->updated_at : "";
        $result['attributes']['created_at'] = $data->created_at ? $data->created_at : "";

        $result['attributes']['items'] = [];
        foreach($data->items()->get() as $item) {
            $result['attributes']['items'][] = $this->itemMapper->single($item);
        }

        return $result;
    }
}
