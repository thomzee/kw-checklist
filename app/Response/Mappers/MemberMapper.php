<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;
use Illuminate\Support\Facades\Storage;

class MemberMapper extends BaseMapper implements MapperContract
{
    public function single($item) {
        $result = [];
        $result['name'] = $item->name ? $item->name : "";
        $result['phone'] = $item->phone ? $item->phone : "";
        $result['email'] = $item->email ? $item->email : "";
        $result['avatar'] = $item->image ? asset(Storage::url($item->image->image)) : "";
        return $result;
    }
}
