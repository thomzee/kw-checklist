<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;

class ItemStatusParentMapper extends BaseMapper implements MapperContract
{
    private $itemStatusMapper;

    public function __construct()
    {
        $this->itemStatusMapper = new ItemStatusMapper();
    }

    public function single($data) {
        $result = [];
        $result['data'] = [];
        foreach ($data as $item) {
            $result['data'][] = $this->itemStatusMapper->single($item);
        }
        return $result['data'];
    }
}
