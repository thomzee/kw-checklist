<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;
use Illuminate\Support\Facades\Storage;

class AuthLoginMapper extends BaseMapper implements MapperContract
{
    public function single($apiToken) {
        $result = [];
        $result['api_token'] = $apiToken ? $apiToken : "";
        return $result;
    }
}
