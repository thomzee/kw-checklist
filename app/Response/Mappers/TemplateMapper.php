<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;

class TemplateMapper extends BaseMapper implements MapperContract
{
    private $templateItemMapper;

    public function __construct()
    {
        $this->templateItemMapper = new TemplateItemMapper();
    }

    public function single($data) {
        $result = [];
        $result['type'] = 'templates';
        $result['id'] = $data ? $data->id : "";
        $result['attributes']['name'] = $data->name ? $data->name : "";
        $result['attributes']['checklist']['description'] = $data->description ? $data->description : "";
        $result['attributes']['checklist']['due_interval'] = $data->due_interval ? $data->due_interval : "";
        $result['attributes']['checklist']['due_unit'] = $data->due_unit ? $data->due_unit : "";
        $result['attributes']['items'] = [];

        foreach($data->templateItems()->get() as $item) {
            $result['attributes']['items'][] = $this->templateItemMapper->single($item);
        }

        return $result;
    }
}
