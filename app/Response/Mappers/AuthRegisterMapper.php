<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;
use Illuminate\Support\Facades\Storage;

class AuthRegisterMapper extends BaseMapper implements MapperContract
{
    public function single($item) {
        $result = [];
        $result['name'] = $item->name ? $item->name : "";
        $result['email'] = $item->email ? $item->email : "";
        return $result;
    }
}
