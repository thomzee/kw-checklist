<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;

class ItemMapper extends BaseMapper implements MapperContract
{
    private $templateItemMapper;

    public function __construct()
    {
        $this->templateItemMapper = new TemplateItemMapper();
    }

    public function single($data) {
        $result = [];
        $result['type'] = 'items';
        $result['id'] = $data ? $data->id : "";
        $result['attributes']['task_id'] = $data->task_id ? $data->task_id : "";
        $result['attributes']['description'] = $data->description ? $data->description : "";
        $result['attributes']['is_completed'] = $data->is_completed ? $data->is_completed : "";
        $result['attributes']['due'] = $data->due ? $data->due : "";
        $result['attributes']['urgency'] = $data->urgency ? $data->urgency : "";
        $result['attributes']['completed_at'] = $data->completed_at ? $data->completed_at : "";
        $result['attributes']['updated_by'] = $data->updated_by ? $data->updated_by : "";
        $result['attributes']['updated_at'] = $data->updated_at ? $data->updated_at : "";
        $result['attributes']['created_at'] = $data->created_at ? $data->created_at : "";
        $result['attributes']['assignee_id'] = $data->assignee_id ? $data->assignee_id : "";
        $result['attributes']['created_by'] = $data->created_by ? $data->created_by : "";

        return $result;
    }
}
