<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;
use Illuminate\Support\Facades\Storage;

class TemplateItemMapper extends BaseMapper implements MapperContract
{
    public function single($data) {
        $result = [];
        $result['description'] = $data->description ? $data->description : "";
        $result['urgency'] = $data->urgency ? $data->urgency : "";
        $result['due_interval'] = $data->due_interval ? $data->due_interval : "";
        $result['due_unit'] = $data->due_unit ? $data->due_unit : "";
        return $result;
    }
}
