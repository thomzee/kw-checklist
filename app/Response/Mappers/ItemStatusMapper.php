<?php


namespace App\Response\Mappers;


use App\Response\BaseMapper;
use App\Response\MapperContract;

class ItemStatusMapper extends BaseMapper implements MapperContract
{

    public function single($data) {
        $result = [];
        $result['id'] = $data->id;
        $result['item_id'] = $data->id;
        $result['is_completed'] = $data->is_completed;
        $result['checklist_id'] = $data->checklist_id;
        return $result;
    }
}
