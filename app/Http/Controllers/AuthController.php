<?php

namespace App\Http\Controllers;

use App\Response\Mappers\AuthLoginMapper;
use App\Response\Mappers\AuthRegisterMapper;
use App\Response\Response;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    private $user;
    private $response;
    private $authRegisterMapper;
    private $authLoginMapper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = new User();
        $this->response = new Response();
        $this->authRegisterMapper = new AuthRegisterMapper();
        $this->authLoginMapper = new AuthLoginMapper();
    }

    /**
     * Register new user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user = $this->user->create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return response()->json($this->response->single($user, $this->authRegisterMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * User login and returns api_token.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user = $this->user->where('email', $request->email)->first();
            if (!$user) throw new \Exception('Email and/or password combination doesn\'t exist.');

            $apiToken = "";
            if (Hash::check($request->password, $user->password)) {
                $apiToken = base64_encode(str_random(40));
                $user->update([
                    'api_token' => $apiToken
                ]);
            } else {
                throw new \Exception('Email and/or password combination doesn\'t exist.');
            }

            return response()->json($this->response->single($apiToken, $this->authLoginMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
