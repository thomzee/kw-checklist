<?php

namespace App\Http\Controllers;

use App\Response\Mappers\ChecklistMapper;
use App\Response\Response;
use App\Service\ChecklistService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Validator;

class ChecklistController extends Controller
{
    private $response;
    private $checklistMapper;
    private $checklistService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->response = new Response();
        $this->checklistMapper = new ChecklistMapper();
        $this->checklistService = new ChecklistService();
    }

    public function index()
    {
        try {
            $data = $this->checklistService->paging(app('request')->get('limit'));
            return response()->json($this->response->list($data, $this->checklistMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detail($id)
    {
        try {
            $data = $this->checklistService->findOrFail($id);
            return response()->json($this->response->single($data, $this->checklistMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.*' => 'required',
                'data.attributes.*' => 'required',
                'data.attributes.object_domain' => 'required',
                'data.attributes.object_id' => 'required|numeric',
                'data.attributes.description' => 'required',
                'data.attributes.due' => 'date_format:Y-m-d H:i:s',
                'data.attributes.urgency' => 'numeric',
                'data.attributes.task_id' => 'numeric',
                'data.attributes.items' => 'sometimes|array',
                'data.attributes.items.*' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $checklist = $this->checklistService->createChecklist($request);
            $this->checklistService->createChecklistItem($checklist, $request);
            DB::commit();

            return response()->json($this->response->single($checklist, $this->checklistMapper), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.*' => 'required',
                'data.attributes.*' => 'required',
                'data.attributes.object_domain' => 'required',
                'data.attributes.object_id' => 'required|numeric',
                'data.attributes.description' => 'required',
                'data.attributes.due' => 'date_format:Y-m-d H:i:s',
                'data.attributes.urgency' => 'numeric',
                'data.attributes.task_id' => 'numeric',
                'data.attributes.items.*.description' => 'required',
                'data.attributes.items.*.urgency' => 'numeric',
                'data.attributes.items.*.due' => 'date_format:Y-m-d H:i:s',
                'data.attributes.items.*.assignee_id' => 'numeric',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = $this->checklistService->findOrFail($id);
            $this->checklistService->updateChecklist($data, $request);
            DB::commit();

            return response()->json($this->response->single($data, $this->checklistMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($id)
    {
        try {
            $data = $this->checklistService->findOrFail($id);
            $this->checklistService->delete($data);
            return response()->json($this->response->success(), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
