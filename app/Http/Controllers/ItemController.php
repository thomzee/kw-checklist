<?php

namespace App\Http\Controllers;

use App\Response\Mappers\ChecklistMapper;
use App\Response\Mappers\ItemIndexMapper;
use App\Response\Mappers\ItemStatusParentMapper;
use App\Response\Response;
use App\Service\ChecklistService;
use App\Service\ItemService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    private $response;
    private $checklistMapper;
    private $checklistService;
    private $itemService;
    private $itemStatusParentMapper;
    private $itemIndexMapper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->response = new Response();
        $this->checklistMapper = new ChecklistMapper();
        $this->checklistService = new ChecklistService();
        $this->itemService = new ItemService();
        $this->itemStatusParentMapper = new ItemStatusParentMapper();
        $this->itemIndexMapper = new ItemIndexMapper();
    }

    public function index($id)
    {
        try {
            $data = $this->checklistService->findOrFail($id);
            return response()->json($this->response->single($data, $this->itemIndexMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detail($checklistId, $itemId)
    {
        try {
            $data = $this->itemService->findByChecklistIdAndItemId($checklistId, $itemId);
            return response()->json($this->response->single($data, $this->checklistMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.*' => 'required',
                'data.attribute.*' => 'required',
                'data.attribute.description' => 'required',
                'data.attribute.due' => 'date_format:Y-m-d H:i:s',
                'data.attribute.urgency' => 'numeric',
                'data.attribute.task_id' => 'numeric',
                'data.attribute.is_completed' => 'boolean',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $checklist = $this->checklistService->findOrFail($id);
            $this->itemService->createChecklistItem($checklist, $request);
            DB::commit();

            return response()->json($this->response->single($checklist, $this->checklistMapper), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($checklistId, $itemId, Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.*' => 'required',
                'data.attribute.*' => 'required',
                'data.attribute.description' => 'required',
                'data.attribute.due' => 'date_format:Y-m-d H:i:s',
                'data.attribute.urgency' => 'numeric',
                'data.attribute.task_id' => 'numeric',
                'data.attribute.is_completed' => 'boolean',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $checklist = $this->checklistService->findOrFail($checklistId);
            $item = $this->itemService->findByChecklistIdAndItemId($checklistId, $itemId);
            $this->checklistService->updateChecklist($item, $request);
            DB::commit();

            return response()->json($this->response->single($checklist, $this->checklistMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($checklistId, $itemId)
    {
        try {
            $data = $this->itemService->findByChecklistIdAndItemId($checklistId, $itemId);
            $this->itemService->delete($data);
            return response()->json($this->response->success(), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function complete(Request $request)
    {
        Db::beginTransaction();
        try {
            $data = $this->itemService->changeAllStatus($request, true);
            DB::commit();
            return response()->json($this->response->single($data, $this->itemStatusParentMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function incomplete(Request $request)
    {
        Db::beginTransaction();
        try {
            $data = $this->itemService->changeAllStatus($request, false);
            DB::commit();
            return response()->json($this->response->single($data, $this->itemStatusParentMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
