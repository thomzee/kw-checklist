<?php

namespace App\Http\Controllers;

use App\Response\Mappers\TemplateMapper;
use App\Response\Response;
use App\Service\TemplateService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    private $response;
    private $templateMapper;
    private $templateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->response = new Response();
        $this->templateMapper = new TemplateMapper();
        $this->templateService = new TemplateService();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $data = $this->templateService->paging(app('request')->get('limit'));
            return response()->json($this->response->list($data, $this->templateMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function detail($id)
    {
        try {
            $data = $this->templateService->findOrFail($id);
            return response()->json($this->response->single($data, $this->templateMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.attributes.name' => 'required',
                'data.attributes.checklist.description' => 'required',
                'data.attributes.checklist.due_interval' => 'numeric',
                'data.attributes.checklist.due_unit' => 'in:hour,minute',
                'data.attributes.items.*.description' => 'required',
                'data.attributes.items.*.urgency' => 'numeric',
                'data.attributes.items.*.due_interval' => 'numeric',
                'data.attributes.items.*.due_unit' => 'in:hour,minute',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $template = $this->templateService->createTemplate($request);
            $this->templateService->createTemplateItem($template, $request);
            DB::commit();

            return response()->json($this->response->single($template, $this->templateMapper), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'data.attributes.name' => 'required',
                'data.attributes.checklist.description' => 'required',
                'data.attributes.checklist.due_interval' => 'numeric',
                'data.attributes.checklist.due_unit' => 'in:hour,minute',
                'data.attributes.items.*.description' => 'required',
                'data.attributes.items.*.urgency' => 'numeric',
                'data.attributes.items.*.due_interval' => 'numeric',
                'data.attributes.items.*.due_unit' => 'in:hour,minute',
            ]);

            if ($validator->fails()) {
                return response()->json($this->response->validationFail($validator->errors()->all()), JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = $this->templateService->findOrFail($id);
            $updatedTemplate = $this->templateService->updateTemplate($data, $request);
            $this->templateService->updateTemplateItem($updatedTemplate, $request);
            DB::commit();

            return response()->json($this->response->single($data, $this->templateMapper), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $data = $this->templateService->findOrFail($id);
            $this->templateService->delete($data);
            return response()->json($this->response->success(), JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json($this->response->error($e), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
